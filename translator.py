import re
import sys
from collections import deque
from isa import Opcode, write_code

RESERVED = 'RESERVED'

token_patterns = [
    (r'[ \n\t]+', None),
    (r'#[^\n]*', None),
    (r'\=\=', RESERVED),
    (r'\=', RESERVED),
    (r'\(', RESERVED),
    (r'\)', RESERVED),
    (r';', RESERVED),
    (r'\{', RESERVED),
    (r'\}', RESERVED),
    (r'\+', RESERVED),
    (r'\-', RESERVED),
    (r'\*', RESERVED),
    (r'/', RESERVED),
    (r'%',RESERVED),
    (r'!=',RESERVED),
    (r'\&',RESERVED),
    (r'\|',RESERVED),
    (r'<=',RESERVED),
    (r'>=',RESERVED),
    (r'\>',RESERVED),
    (r'\<',RESERVED),
    (r'if',RESERVED),
    (r'else',RESERVED),
    (r'while',RESERVED),
    (r'break',RESERVED),
    (r'continue',RESERVED),
    (r'input',RESERVED),
    (r'printf', RESERVED),
    (r'print',RESERVED),
    (r'read',RESERVED),
    (r'\[',RESERVED),
    (r'\]',RESERVED),
    (r'\bint\b',RESERVED),
    (r'\bstring\b',RESERVED),
    (r'[0-9]+',RESERVED),
    (r'[A-Za-z][A-Za-z0-9v_]*',RESERVED),
    (r'"[A-Za-z0-9_ !@#$%^&*-=+}\*\-\{\}\[\]\'\"\;\:\/\?\.\>\<\,\|\\/ ]*"',RESERVED),
    (r"'[A-Za-z0-9_ !@#$%^&*-=+}\*\-\{\}\[\]\'\"\;\:\/\?\.\>\<\,\|\\/ ]*'", RESERVED),
]

operators = {
    "=": 0,
    "==": 1,
    "!=": 1,
    "<": 1,
    ">": 1,
    ">=": 1,
    "<=": 1,
    "&": 1,
    "|": 1,
    "+": 1,
    "-": 1,
    "*": 2,
    "/": 2,
    "%": 2,
    "@": 3
}
parenthesis = {
    "[": 4,
    "]": -4
}
brackets = {
    "(": 4,
    ")": -4
}

curly_braces = {
    "{": 4,
    "}": -4
}


def pre_process(raw: str) -> str:
    lines = []
    for line in raw.split("\n"):
        # remove comments
        comment_idx = line.find("#")
        if comment_idx != -1:
            line = line[:comment_idx]
        # remove leading spaces
        line = line.strip()

        lines.append(line)

    text = " ".join(lines)
    text = re.sub(r"\n", " ", text)

    return text


def lex(characters):
    pos = 0
    tokens = []
    while pos < len(characters):
        match = None
        for token_expr in token_patterns:
            pattern, tag = token_expr
            regex = re.compile(pattern)
            match = regex.match(characters, pos)
            if match:
                text = match.group(0)
                if tag:
                    if text == '[':
                        tokens.append('@')
                    token = text
                    tokens.append(token)
                break
        if not match:
            sys.stderr.write('Illegal character: %s\n' % characters[pos])
            sys.exit(1)
        else:
            pos = match.end(0)
    return tokens


class ASTParserUnit:
    """
    Intermediate representation
    """

    def __init__(self) -> None:
        self.terms = deque()
        self.nesting_level = deque()

    def getCurrentTerm(self) -> deque:
        max_level = len(self.nesting_level)
        if max_level == 0:
            return self.terms
        term = self.terms[self.nesting_level[0]]
        for i in range(1, max_level):
            if isinstance(term[self.nesting_level[i]], list):
                term = term[self.nesting_level[i]]
            else:
                return term
        return term

    def driveIn(self):
        term = self.getCurrentTerm()
        term.append([])
        self.nesting_level.append(len(term) - 1)

    def addToken(self, token):
        self.getCurrentTerm().append(token)

    def driveOut(self):
        term = self.getCurrentTerm()
        if term and not term[-1]:
            term.pop()
        self.nesting_level.pop()

    def exportTerms(self):
        if self.terms and not self.terms[-1]:
            self.terms.pop()
        return list(self.terms)


def buildAST(tokens: list):
    parseUnit = ASTParserUnit()
    parseUnit.driveIn()

    for token in tokens:
        if token in ['if','while','input','print']:
            parseUnit.addToken(token)
        elif token == '(':
            parseUnit.driveIn()
        elif token == '{':
            parseUnit.driveIn()
            parseUnit.driveIn()
        elif token == ')':
            parseUnit.driveOut()
        elif token == '}':
            parseUnit.driveOut()
            parseUnit.driveOut()
            parseUnit.driveOut()
            parseUnit.driveIn()
        elif token == ';':
            parseUnit.driveOut()
            parseUnit.driveIn()
        else:
            parseUnit.addToken(token)

    return parseUnit.exportTerms()


PRIOR = 0
OP = 1
INDEX = 1


def last_min_op(exprs):
    last = 0
    for expr_idx, expr in enumerate(exprs):
        if expr[PRIOR] <= exprs[last][PRIOR]:
            last = expr_idx
    return last


def parse_expression(tokens):
    while any(map(lambda token: isinstance(token, list), tokens)):
        linear_token = deque()
        for token in tokens:
            if isinstance(token, list):
                linear_token.append("(")
                linear_token.extend(token)
                linear_token.append(')')
            else:
                linear_token.append(token)
        tokens = linear_token
    operation_priority = 0
    expr = deque()
    ops: deque[tuple] = deque()
    for token_idx, token in enumerate(tokens):
        if token in brackets:
            operation_priority += brackets[token]
        elif token in parenthesis:
            operation_priority += parenthesis[token]
        elif token in operators:
            priority = operation_priority + operators[token]
            expr.append((priority, token))
        else:
            expr.append(token)
    for token_idx, token in enumerate(expr):
        if isinstance(token, tuple):
            ops.append((token[0], token_idx))
    expr = list(expr)

    def resolve(ops: list[tuple]):
        op_ptr = last_min_op(ops)
        if op_ptr == len(ops) - 1:
            right = expr[ops[op_ptr][INDEX] + 1]
        else:
            right = resolve(ops[op_ptr + 1:])
        if op_ptr == 0:
            left = expr[ops[op_ptr][INDEX] - 1]
        else:
            left = resolve(ops[:op_ptr])
        return expr[ops[op_ptr][INDEX]][OP], left, right

    return resolve(list(ops)) if len(ops) else tokens[0]


class Translator:
    ARGS = "args"
    OPCODE = "opcode"
    FISH = "undefined_address"

    def __init__(self) -> None:
        self.program = deque()
        self.vars = {}
        self.memory = deque()
        self.data_address = 0
        [self.X0, self.X1, self.X2, self.X3, self.SP] = ["0", "1", "2", "3", "4"]
        self.op = {}
        self.pc = 0
        self.unresolved_addresses = deque()
        for opcode in list(Opcode):
            self.op[opcode.name] = self.generate_commands(opcode)

    def generate_commands(self, opcode):
        def insert_command(rt=None, rla=None, rrb=None):
            args = []
            for arg in rt, rla, rrb:
                if arg is not None:
                    args.append(arg)
            self.program.append(
                {
                    self.OPCODE: opcode,
                    self.ARGS: args
                }
            )
            self.pc += 1

        return insert_command

    def append_unresolved_address(self):
        self.unresolved_addresses.append(self.pc - 1)

    def resolve_address(self):
        address = self.unresolved_addresses.pop()
        self.program[address][self.ARGS][2] = self.pc

    def solve(self, expr):
        if isinstance(expr, tuple):
            operation, left, right = expr
            self.solve(left)
            self.solve(right)
            if operation == '@':
                self.op['ADDI'](self.SP, self.SP, 1)
                self.op['LDR'](self.X2, self.SP)
                self.op['ADDI'](self.SP, self.SP, 1)
                self.op['LDR'](self.X1, self.SP)
                self.op['ADD'](self.X3, self.X1, self.X2)
                self.op['LDR'](self.X1, self.X3)
                self.op['STR'](self.SP, self.X1)
                self.op['SUBI'](self.SP, self.SP, 1)

            else:
                self.op['ADDI'](self.SP, self.SP, 1)
                self.op['LDR'](self.X2, self.SP)
                self.op['ADDI'](self.SP, self.SP, 1)
                self.op['LDR'](self.X1, self.SP)

                if operation == '==':
                    self.op['SEQ'](self.X3, self.X1, self.X2)
                elif operation == '!=':
                    self.op['SNE'](self.X3, self.X1, self.X2)
                elif operation == '>':
                    self.op['SGT'](self.X3, self.X1, self.X2)
                elif operation == '<':
                    self.op['SLT'](self.X3, self.X1, self.X2)
                elif operation == '>=':
                    self.op['SNL'](self.X3, self.X1, self.X2)
                elif operation == '<=':
                    self.op['SNG'](self.X3, self.X1, self.X2)
                elif operation == '|':
                    self.op['OR'](self.X3, self.X1, self.X2)
                elif operation == '&':
                    self.op['AND'](self.X3, self.X1, self.X2)
                elif operation == '+':
                    self.op['ADD'](self.X3, self.X1, self.X2)
                elif operation == '-':
                    self.op['SUB'](self.X3, self.X1, self.X2)
                elif operation == '*':
                    self.op['MUL'](self.X3, self.X1, self.X2)
                elif operation == '/':
                    self.op['DIV'](self.X3, self.X1, self.X2)
                elif operation == '%':
                    self.op['REM'](self.X3, self.X1, self.X2)

                self.op['STR'](self.SP, self.X3)
                self.op['SUBI'](self.SP, self.SP, 1)
        else:
            if expr.isdigit():
                self.op['STRI'](self.SP, int(expr))
                self.op['SUBI'](self.SP, self.SP, 1)
            elif expr in self.vars:
                self.op['LDRI'](self.X1, self.vars[expr])
                self.op['STR'](self.SP, self.X1)
                self.op['SUBI'](self.SP, self.SP, 1)

    def addr(self, addr):
        if isinstance(addr, tuple):
            _, left, right = addr
            self.solve(left)
            self.solve(right)
            self.op['ADDI'](self.SP, self.SP, 1)
            self.op['LDR'](self.X2, self.SP)
            self.op['ADDI'](self.SP, self.SP, 1)
            self.op['LDR'](self.X1, self.SP)
            self.op['ADD'](self.X3, self.X1, self.X2)
            self.op['STR'](self.SP, self.X3)
            self.op['SUBI'](self.SP, self.SP, 1)
        else:
            self.op['ADDI'](self.X1, self.X0, self.vars[addr])
            self.op['STR'](self.SP, self.X1)
            self.op['SUBI'](self.SP, self.SP, 1)

    def allocate(self, term: list):
        if term[0] == 'int' or term[0] == 'string':
            variable = term[1]
            if len(term) > 5 and (term[3], term[5]) == ('[', ']'):
                self.vars[variable] = self.data_address
                self.data_address += 1
                self.memory.append(self.data_address)

                self.data_address += int(term[4])
                if len(term) > 6:
                    if isinstance(term[7], list):
                        inits = list(filter(lambda val: val != ',', term[7]))
                        for i in range(0, len(inits)):
                            if term[0] == 'string' and ((inits[i][0] != "'" and inits[i][-1] != "'") or (
                                    inits[i][0][0] != '"' and inits[i][0][-1] != '"')):
                                raise ValueError
                            elif term[0][0] == 'int':
                                if not inits[i][0].isdigit():
                                    raise ValueError
                        self.memory.extend(map(int, inits))
                        self.memory.extend([0] * (int(term[4][0]) - len(inits)))
                    elif (term[7][0], term[7][-1]) == ("'", "'"):
                        inits = list(map(ord, term[7][1:-1]))
                        self.memory.extend(map(int, inits))
                        self.memory.extend([0] * (int(term[4]) - len(inits)))

                else:
                    self.memory.extend([0] * int(term[4]))
            else:
                self.vars[variable] = self.data_address
                self.data_address += 1
                if len(term) > 2 and term[2] == '=':
                    value = term[3]
                    if value.isdigit():
                        if term[0] == 'string':
                            raise ValueError
                        self.memory.append(int(value))
                    elif value not in self.vars and (value[0], value[-1]) == ("'", "'") or (value[0], value[-1]) == (
                            '"', '"'):
                        self.memory.append(value)
                    else:
                        self.memory.append(self.memory[self.vars[value]])
                else:
                    self.memory.append(0)

    def _translate_(self, term: list):
        if not term or term[0] == 'int' or term[0] == 'string':
            return
        if term[0] in ['if', 'while']:
            keyword = term[0]
            condition = term[1]
            body = term[2]
            condition_start = self.pc
            self.solve(parse_expression(condition))

            self.op['ADDI'](self.SP, self.SP, 1)
            self.op['LDR'](self.X1, self.SP)
            self.op['BEQ'](self.X1, self.X0, self.FISH)
            self.append_unresolved_address()

            for _term_ in body:
                self._translate_(_term_)

            if keyword == 'while':
                self.op['B'](condition_start)
            self.resolve_address()

        elif term[0] == 'input':
            mvalue = parse_expression(term[1])
            self.addr(mvalue)
            self.op['ADDI'](self.SP, self.SP, 1)
            self.op['LDR'](self.X1, self.SP)
            self.op['IN'](self.X3)
            self.op['STR'](self.X1, self.X3)

        elif term[0] == 'printf':
            body = term[1][0]
            if body.isdigit():
                self.op['ADDI'](self.X1, self.X0, body)
            else:
                vvalue = parse_expression(term[1])
                self.solve(vvalue)
                self.op['ADDI'](self.SP, self.SP, 1)
                self.op['LDR'](self.X1, self.SP)
            self.op['OUT'](self.X1)

        elif term[0] == 'print':
            body = term[1][0]
            if (body in self.vars and type(self.memory[self.vars[body]]) == int) or body.isdigit():
                if body.isdigit():
                    self.op['ADDI'](self.X2, self.X0, body)
                    self.op['STR'](self.X1, self.X2)
                    self.op['OUTI'](self.X1)
                else:
                    self.op['LDRI'](self.X2, self.vars[body])
                    self.op['OUTI'](self.X2)

            elif (body in self.vars and self.vars[body]) or ((body[0], body[-1]) == ("'", "'")) or (
                    (body[0], body[-1]) == ('"', '"')):
                self.op['STR'](self.SP, self.X0)
                self.op['ADD'](self.X3, self.SP, self.X0)
                self.op['SUBI'](self.SP, self.SP, 1)
                if body not in self.vars:
                    for character in (body[1:-1]):
                        self.op['ADDI'](self.X1, self.X0, ord(character))
                        self.op['STR'](self.SP, self.X1)
                        self.op['SUBI'](self.SP, self.SP, 1)
                else:
                    for character in (self.memory[self.vars[body]][1:-1]):
                        self.op['ADDI'](self.X1, self.X0, ord(character))
                        self.op['STR'](self.SP, self.X1)
                        self.op['SUBI'](self.SP, self.SP, 1)
                self.op['ADD'](self.SP, self.X3, self.X0)
                self.op['SUBI'](self.SP, self.SP, 1)
                self.op['LDR'](self.X1, self.SP)
                self.op['STR'](self.SP, self.X0)
                self.op['BEQ'](self.X1, self.X0, self.FISH)
                self.append_unresolved_address()
                self.op['STR'](self.X2, self.X1)
                self.op['OUT'](self.X1)
                self.op['B'](self.pc - 6)
                self.resolve_address()
                self.op["ADD"](self.SP, self.X3, self.X0)
            else:
                self.op['LDRI'](self.X2, self.vars[body])
                self.op['LDR'](self.X1, self.X2)
                self.op['BEQ'](self.X1, self.X0, self.FISH)
                self.append_unresolved_address()
                self.op['ADDI'](self.X3, self.X0, self.X3)
                self.op['OUT'](self.X3)
                self.op['STR'](self.X3, self.X1)
                self.op['ADDI'](self.X2, self.X2, 1)
                self.op['B'](self.pc - 5)
                self.resolve_address()

        else:
            operation = term.index('=')
            mvalue = parse_expression(term[:operation])
            expression = parse_expression(term[operation + 1:])
            self.addr(mvalue)
            if (expression[0], expression[-1]) == ("'", "'"):
                self.op['ADDI'](self.SP, self.SP, 1)
                self.op['LDR'](self.X2, self.SP)
                self.op['LDR'](self.X3, self.X2)
                for char in expression[1:-1]:
                    self.op['STRI'](self.X3, ord(char))
                    self.op['ADDI'](self.X3, self.X3, 1)
            else:
                self.solve(expression)
                self.op['ADDI'](self.SP, self.SP, 1)
                self.op['LDR'](self.X2, self.SP)
                self.op['ADDI'](self.SP, self.SP, 1)
                self.op['LDR'](self.X1, self.SP)
                self.op['STR'](self.X1, self.X2)

    def translate(self, terms):
        self.program = deque()
        self.unresolved_addresses = deque()
        for term in terms:
            self.allocate(term)
        self.pc = len(self.memory)
        self.program.extend(list(self.memory))
        for term in terms:
            self._translate_(term)

        self.op['HALT']()
        return list(self.program), self.vars


def run(source, target):
    if not source or not target:
        raise FileNotFoundError

    with open(source, 'rt', encoding='utf-8') as f:
        source = f.read()

    text = pre_process(source)
    tokens = lex(text)
    AST = buildAST(tokens)
    translator = Translator()
    program, _ = translator.translate(AST)
    print("source LoC:", len(source.split()), "code instr:", len(program))
    write_code(target, program)


if __name__ == '__main__':
    run(sys.argv[1], sys.argv[2])
