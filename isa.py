import json
from enum import Enum


class Opcode(str, Enum):
    """
    Opcode
    """

    IN = "IN"
    OUT = "OUT"
    OUTI = "OUTI"

    LDR = 'LDR'
    STR = 'STR'
    LDRI = 'LDRI'
    STRI = 'STRI'

    B = "B"

    BEQ = 'BEQ'
    BNE = "BNE"
    BLT = "BLT"
    BGT = "BGT"
    BNL = 'BNL'
    BNG = 'BNG'

    AND = 'AND'
    OR = 'OR'

    ANDI = 'ANDI'
    ORI = 'ORI'

    ADD = 'ADD'
    SUB = 'SUB'
    MUL = 'MUL'
    DIV = 'DIV'
    REM = 'REM'

    ADDI = 'ADDI'
    SUBI = 'SUBI'
    MULI = 'MULI'
    DIVI = 'DIVI'
    REMI = 'REMI'

    SEQ = 'SEQ'
    SNE = 'SNE'
    SLT = 'SLT'
    SGT = 'SGT'
    SNL = 'SNL'
    SNG = 'SNG'

    SEQI = 'SEQI'
    SNEI = 'SNEI'
    SLTI = 'SLTI'
    SGTI = 'SGTI'
    SNLI = 'SNLI'
    SNGI = 'SNGI'

    HALT = 'HALT'


ops_group = {"branch": {
    Opcode.B,
    Opcode.BEQ,
    Opcode.BNE,
    Opcode.BLT,
    Opcode.BGT,
    Opcode.BNL,
    Opcode.BNG
}, 'immediate': {
    Opcode.ADDI,
    Opcode.SUBI,
    Opcode.MULI,
    Opcode.DIVI,
    Opcode.REMI,
    Opcode.ANDI,
    Opcode.ORI,

    Opcode.SEQI,
    Opcode.SNEI,
    Opcode.SLTI,
    Opcode.SGTI,
    Opcode.SNLI,
    Opcode.SNGI,

    Opcode.LDRI,
    Opcode.STRI
}, 'arithmetic': {
    Opcode.AND,
    Opcode.OR,

    Opcode.ADDI,
    Opcode.SUBI,
    Opcode.MULI,
    Opcode.DIVI,
    Opcode.REMI,

    Opcode.ADD,
    Opcode.SUB,
    Opcode.MUL,
    Opcode.DIV,
    Opcode.REM,

    Opcode.SEQ,
    Opcode.SNE,
    Opcode.SLT,
    Opcode.SGT,
    Opcode.SNL,
    Opcode.SNG,

    Opcode.SEQI,
    Opcode.SNEI,
    Opcode.SLTI,
    Opcode.SGTI,
    Opcode.SNLI,
    Opcode.SNGI,
}}


def write_code(filename: str, program: list):
    """Записать машинный код в файл"""
    with open(filename, 'w+', encoding="utf-8") as file:
        file.write(json.dumps(program, indent=4))


def read_code(filename: str) -> list:
    """Прочесть машинный код из файла"""
    with open(filename,encoding="utf-8") as file:
        return json.loads(file.read())
