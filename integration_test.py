import os
import tempfile
import pytest
import machine
import translator


@pytest.mark.golden_test("./golden/*.yml")
def test_translator_golden(golden):
    with tempfile.TemporaryDirectory() as tmpdirname:
        source = os.path.join(tmpdirname, "source.alg")
        target = os.path.join(tmpdirname, "target.json")

        with open(source, "w", encoding='utf-8') as file:
            file.write(golden['source'])

        translator.run(source, target)

        with open(target, 'r', encoding='utf-8') as file:
            assert file.read() == golden['code']


@pytest.mark.golden_test("./golden/*.yml")
def test_machine_golden(golden, capfd):
    with tempfile.TemporaryDirectory() as tmpdirname:
        source = os.path.join(tmpdirname, "source.json")
        input_stream = os.path.join(tmpdirname, 'input.txt')
        log = os.path.join(tmpdirname, 'target.log')

        with open(source, "w", encoding='utf-8') as file:
            file.write(golden['code'])
        with open(input_stream, "w", encoding="utf-8") as file:
            file.write(golden["input"])
        open(log,'a',encoding='utf-8')

        machine.run(source, input_stream, log)
        stdout, stderr = capfd.readouterr()

        assert stdout == golden['output']
